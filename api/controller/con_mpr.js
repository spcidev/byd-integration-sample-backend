const request = require ('request');

let body = 
`<?xml version="1.0" encoding="utf-8"?>
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:glob="http://sap.com/xi/SAPGlobal20/Global">
<soapenv:Header/>
<soapenv:Body>
    <n0:PurchaseRequestMaintainBundleRequest xmlns:n0="http://sap.com/xi/SAPGlobal20/Global">
	<BasicMessageHeader/>
	<PurchaseRequestMaintainBundle actionCode="01">
		<Item actionCode="01">
<ProductKeyItem>
                  <ProductID>DSSP040/02B/BLACK</ProductID>
               </ProductKeyItem>
<ProductDescription></ProductDescription>
			
            <ProductCategoryKeyItem>
                  <ProductCategoryInternalID></ProductCategoryInternalID>
            </ProductCategoryKeyItem>
			<TypeCode>18</TypeCode>
<directMaterialIndicator>1</directMaterialIndicator>
			<ShipToLocationID>D17</ShipToLocationID>
			
			<Quantity unitCode="KGM">1500</Quantity>
			<CompanyIDParty>
				<PartyID>M000000011</PartyID>
			</CompanyIDParty>
			<RecipientIDParty>
				<PartyID></PartyID>
			</RecipientIDParty>
			<BuyerResponsibleIDParty>
				<PartyID>240192</PartyID>
			</BuyerResponsibleIDParty>
			<SupplierIDParty>
				<PartyID></PartyID>
			</SupplierIDParty>
            <DeliveryPeriod>
<StartDateTime timeZoneCode="UTC">2022-01-28T12:00:00.1234567Z</StartDateTime>
</DeliveryPeriod>
		</Item>

<Item actionCode="01">
<ProductKeyItem>
                  <ProductID>FIN1000</ProductID>
               </ProductKeyItem>
<ProductDescription></ProductDescription>
			
            <ProductCategoryKeyItem>
                  <ProductCategoryInternalID></ProductCategoryInternalID>
            </ProductCategoryKeyItem>
			<TypeCode>18</TypeCode>
<directMaterialIndicator>1</directMaterialIndicator>
			<ShipToLocationID>D17</ShipToLocationID>
			
			<Quantity unitCode="KGM">1500</Quantity>
			<CompanyIDParty>
				<PartyID>M000000011</PartyID>
			</CompanyIDParty>
			<RecipientIDParty>
				<PartyID></PartyID>
			</RecipientIDParty>
			<BuyerResponsibleIDParty>
				<PartyID>240192</PartyID>
			</BuyerResponsibleIDParty>
			<SupplierIDParty>
				<PartyID></PartyID>
			</SupplierIDParty>
            <DeliveryPeriod>
<StartDateTime timeZoneCode="UTC">2022-01-28T12:00:00.1234567Z</StartDateTime>
</DeliveryPeriod>
		</Item>

<Item actionCode="01">
<ProductKeyItem>
                  <ProductID>102SUP1035</ProductID>
               </ProductKeyItem>
<ProductDescription></ProductDescription>
			
            <ProductCategoryKeyItem>
                  <ProductCategoryInternalID></ProductCategoryInternalID>
            </ProductCategoryKeyItem>
			<TypeCode>18</TypeCode>
<directMaterialIndicator>1</directMaterialIndicator>
			<ShipToLocationID>D17</ShipToLocationID>
			
			<Quantity unitCode="E19">1500</Quantity>
			<CompanyIDParty>
				<PartyID>M000000011</PartyID>
			</CompanyIDParty>
			<RecipientIDParty>
				<PartyID></PartyID>
			</RecipientIDParty>
			<BuyerResponsibleIDParty>
				<PartyID>240192</PartyID>
			</BuyerResponsibleIDParty>
			<SupplierIDParty>
				<PartyID></PartyID>
			</SupplierIDParty>
            <DeliveryPeriod>
<StartDateTime timeZoneCode="UTC">2022-01-28T12:00:00.1234567Z</StartDateTime>
</DeliveryPeriod>
		</Item>

<Item actionCode="01">
<ProductKeyItem>
                  <ProductID>102WRP1445</ProductID>
               </ProductKeyItem>
<ProductDescription></ProductDescription>
			
            <ProductCategoryKeyItem>
                  <ProductCategoryInternalID></ProductCategoryInternalID>
            </ProductCategoryKeyItem>
			<TypeCode>18</TypeCode>
<directMaterialIndicator>1</directMaterialIndicator>
			<ShipToLocationID>D17</ShipToLocationID>
			
			<Quantity unitCode="E19">1500</Quantity>
			<CompanyIDParty>
				<PartyID>M000000011</PartyID>
			</CompanyIDParty>
			<RecipientIDParty>
				<PartyID></PartyID>
			</RecipientIDParty>
			<BuyerResponsibleIDParty>
				<PartyID>240192</PartyID>
			</BuyerResponsibleIDParty>
			<SupplierIDParty>
				<PartyID></PartyID>
			</SupplierIDParty>
            <DeliveryPeriod>
<StartDateTime timeZoneCode="UTC">2022-01-28T12:00:00.1234567Z</StartDateTime>
</DeliveryPeriod>
		</Item>

<Item actionCode="01">
<ProductKeyItem>
                  <ProductID>102LBL0750</ProductID>
               </ProductKeyItem>
<ProductDescription></ProductDescription>
			
            <ProductCategoryKeyItem>
                  <ProductCategoryInternalID></ProductCategoryInternalID>
            </ProductCategoryKeyItem>
			<TypeCode>18</TypeCode>
<directMaterialIndicator>1</directMaterialIndicator>
			<ShipToLocationID>D17</ShipToLocationID>
			
			<Quantity unitCode="E19">1500</Quantity>
			<CompanyIDParty>
				<PartyID>M000000011</PartyID>
			</CompanyIDParty>
			<RecipientIDParty>
				<PartyID></PartyID>
			</RecipientIDParty>
			<BuyerResponsibleIDParty>
				<PartyID>240192</PartyID>
			</BuyerResponsibleIDParty>
			<SupplierIDParty>
				<PartyID></PartyID>
			</SupplierIDParty>
            <DeliveryPeriod>
<StartDateTime timeZoneCode="UTC">2022-01-28T12:00:00.1234567Z</StartDateTime>
</DeliveryPeriod>
		</Item>
        
	</PurchaseRequestMaintainBundle>
</n0:PurchaseRequestMaintainBundleRequest>
</soapenv:Body>
</soapenv:Envelope>`

// module.exports.sendPurchaseRequest = async function(req, res) {
// var postRequest = {
//     hostname: 'https://my355904.sapbydesign.com',
//     path: '/sap/bc/srt/scs/sap/managepurchaserequestin',
//     auth: '_AOS0001:@dd0nP@$$',
//     method: 'POST', 
//     headers: {
//         'Content-Type': 'text/xml;charset=UTF-8',
//         'Content-Length': xmlData.byteLength(body)
//     }

// const option = {
//     'auth': {
//         'user': '_AOS0001',
//         'pass': '@dd0nP@$$',
//         'sendImmediately': false
//       }
// } 
// _AOS0001:@dd0nP@$$@
request({
    method: 'POST',
    uri: 'https://my355904.sapbydesign.com/sap/bc/srt/scs/sap/managepurchaserequestin',
    // path: '',
    auth: {
        'user': '_AOS0001',
        'pass': '@dd0nP@$$',
        'sendImmediately': false
    },
    headers: {
        'Content-Type': 'text/xml'
      },
    body: body,
},
 function (error, response, body) {
  console.error('error:', error); // Print the error if one occurred
  console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
  console.log('body:', body); // Print the HTML for the Google homepage.
})
