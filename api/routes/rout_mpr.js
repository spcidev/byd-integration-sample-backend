const express = require('express');
const sendPurchaseRequestCtrl  = require('../controller/con_mpr');
const router = express.Router();


router.post('/send/mpr', (req, res) => {
    sendPurchaseRequestCtrl.sendPurchaseRequest().then(result => res.send(result));
})


module.exports = router;