const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser');
const mprRoutes = require('./api/routes/rout_mpr');


const app = express();
app.use(cors())
app.use(bodyParser.json());

app.use('/mpr', mprRoutes);

app.listen(process.env.PORT || 1000, () => {
    console.log(`API is now online on port ${ process.env.PORT || 1000 }`)
})
